<?php

namespace App\Http\Controllers;

use App\Models\Voucher;

class VoucherController extends Controller
{
	// public function voucher($code, Voucher $voucher)
	// {
	// 	$voucher = $voucher->withCode($code)->first();
	// 	dd($voucher->balance);
	// }

	public function voucher()
	{
		// $voucher = Voucher::where('code', $code)->get();
		$voucher = Voucher::orderBy('code', 'asc')->get();
		return $voucher;
	}
}